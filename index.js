const app = require('express')();

var animalsRoute = require('./routes/Animals');
var natureRoute = require('./routes/Nature');
var transportRoute = require('./routes/Transport');
var foodsRoute = require('./routes/Food');
var technologyRoute = require('./routes/Technology');

// Animals routes
app.use('/animals', animalsRoute.getAllAnimals)
app.use('/animals/:name', animalsRoute.findByName)
app.use('/animals/:name', animalsRoute.deleteByName)

// // Foods routes
app.use('/foods', foodsRoute.getAllFoods)
app.use('/foods/:name', foodsRoute.findByName)
app.use('/foods/:name', foodsRoute.deleteByName)

// Nature Routes
app.use('/nature', natureRoute.getAllNature)
app.use('/nature/:name', natureRoute.findByName)
app.use('/nature/:name', natureRoute.deleteByName)

// transport Routes
app.use('/transport', transportRoute.getAllTransport)
app.use('/transport/:name', transportRoute.findByName)
app.use('/transport/:name', transportRoute.deleteByName)

// Tech Routes
app.use('/technology', technologyRoute.getAllTech)
app.use('/technology/:name', technologyRoute.findByName)
app.use('/technology/:name', technologyRoute.deleteByName)

app.listen(process.env.PORT || 8000, () => {
    console.log("Server started on port 8000!")
})

