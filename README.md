### Task 1
1. To design an API for the data set given. --> [JSON Data](https://s3.ap-south-1.amazonaws.com/scapic-others/json/models.json)
2. Used Postgres as it was mentioned that you're in affinity with Postgres compared to other relational databases.
3. Hosted on Heroku with Postgres as an add on plugin.
4. [https://postgres-scapic.herokuapp.com]() is the entry point of the backend.

### Routes

1. [/animals](https://postgres-scapic.herokuapp.com/animals)
2. [/foods](https://postgres-scapic.herokuapp.com/foods)
3. [/furniture](https://postgres-scapic.herokuapp.com/furniture)
4. [/nature](https://postgres-scapic.herokuapp.com/nature)
5. [/transport](https://postgres-scapic.herokuapp.com/transport)
6. [/technology](https://postgres-scapic.herokuapp.com/technology)

### Sub Routes to get additional properties
1. #### Animals
    1. ```/animals/:name```, for example if ```name=cow``` the route would be [/animals/cow](https://postgres-scapic.herokuapp.com/animals/cow)
    2. ```/animals/:name/:property```, for example if ```name=cow``` and ```property=thumb``` the route would be [/animals/cow/thumb](https://postgres-scapic.herokuapp.com/animals/cow/thumb)
    3. ```property``` can be either ```name, mtl, obj, thumb```

2. #### Foods
    1. ```/foods/:name```, for example if ```name=corn``` the route would be [/foods/corn](https://postgres-scapic.herokuapp.com/foods/corn)
    2. ```/foods/:name/:property```, if the ```name=corn``` and ```property=obj``` the route would be [/foods/corn/obj](https://postgres-scapic.herokuapp.com/foods/corn/obj)


3. #### Transport
    1. ```/transport/:name```, for example if ```name=jet``` the route would be [/transport/jet](https://postgres-scapic.herokuapp.com/transport/jet)
    2. ```/transport/:name/:property```, if the ```name=jet``` and ```property=obj``` the route would be [/transport/jet/obj](https://postgres-scapic.herokuapp.com/transport/jet/obj)    

4. #### Nature
    1. ```/nature/:name```, for example if ```name=cloud``` the route would be [/nature/cloud](https://postgres-scapic.herokuapp.com/nature/cloud)
    2. ```/nature/:name/:property```, if the ```name=cloud``` and ```property=obj``` the route would be [/nature/cloud/obj](https://postgres-scapic.herokuapp.com/transport/cloud/obj)    


The same applies for all the remaining categories.

