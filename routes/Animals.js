const express = require('express')
const router = express.Router()

const DbConfig = require('../config/DbConfig');
const pool = DbConfig.pool;

let individualComponent;

// category to get animals data
var getAllAnimals = router.get('/', (req, res) => {
    pool.query('SELECT * FROM animals;', (err, resultSet) => {
        if(err) {
            throw err;
        }
        res.json(resultSet.rows)
    })

})

var deleteByName = router.delete('/:name', (req, res) => {
    var query = "DELETE FROM animals WHERE name = '" + req.params.name + "'";
    pool.query(query, (err, resultSet) => {
        if(err) {
            throw err;
        }
        res.json(resultSet.rows)
    })
})

var findByName = router.get("/:name", (req, res) => {

    var query = "SELECT * FROM animals WHERE name = '" + req.params.name + "'";
    pool.query(query, (err, resultSet) => {
        if(err) {
            throw err;
        }
        res.json(resultSet.rows)
        individualComponent = resultSet
    })
})

var findDetailsByName = router.get("/:name/:property", (req, res) => {
    let property = req.params.property
    res.json(individualComponent.rows[0][property])
})

module.exports = {
    getAllAnimals: getAllAnimals,
    findByName: findByName,
    deleteByName: deleteByName,
    findDetailsByName: findDetailsByName
}
